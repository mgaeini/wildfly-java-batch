/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.chunk.demo;

import java.util.List;
import javax.batch.api.chunk.AbstractItemWriter;
import javax.inject.Named;

/**
 * @author mohsen
 */
@Named
public class MyItemWriter extends AbstractItemWriter {

    public MyItemWriter(){
        
    }
    
    @Override
    public void writeItems(List list) {
        System.out.println("writeItems: " + list);
    }
}
