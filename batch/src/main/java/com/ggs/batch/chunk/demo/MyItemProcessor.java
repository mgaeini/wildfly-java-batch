/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.chunk.demo;


import javax.batch.api.chunk.ItemProcessor;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author mohsen
 */
@Named
public class MyItemProcessor implements ItemProcessor {

    public static int totalProcessors = 0;
    private int processorId;

    @Inject
    JobContext context;

    public MyItemProcessor() {
        processorId = ++totalProcessors;
    }

    @Override
    public MyOutputRecord processItem(Object t) {
        System.out.format("processItem (%d): %s\n", processorId, t);

        return (((MyInputRecord) t).getId() % 2 == 0) ? null : new MyOutputRecord(((MyInputRecord) t).getId() * 2);
    }
}
