/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.chunk.demo;

import java.util.Properties;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;


@RunWith(Arquillian.class)
public class BatchChunkPartitionTest {
    
    @Inject
    private NameResolver resolver;

    @Deployment
    public static JavaArchive createDeployment() {
        JavaArchive jar = ShrinkWrap.create(JavaArchive.class)
                .addClass(NameResolver.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"))
                .addAsResource("META-INF/batch-jobs/myjob.xml");
        System.out.println(jar.toString(true));
        return jar;
    }

   
    @Test
    public void testBatchChunkPartition() throws Exception {
        System.out.println("my test running ..."+resolver.resolveName("mohsen"));
        String resolveName = resolver.resolveName("mohsen");
        Assert.assertEquals("mohsen ????", resolveName);
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        jobOperator.start("myjob", new Properties());
        Assert.assertTrue(true);
    }
}
