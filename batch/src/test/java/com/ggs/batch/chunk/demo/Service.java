/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.chunk.demo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 *
 * @author mohsen
 */
@Path("/book")
public class Service {
    
    
    @GET
    @Path("/get")
    public String getBook(@QueryParam("name") String name){
        return "this is a good book: "+name;
    }
    
}
