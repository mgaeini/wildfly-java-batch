/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import com.ggs.batchapp.util.BatchTestHelper;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.JobExecution;
import javax.batch.runtime.Metric;
import javax.batch.runtime.StepExecution;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(Arquillian.class)
public class partitionChunkTest {

    private static final Logger LOG = LoggerFactory.getLogger(partitionChunkTest.class);

    
    
    
    @Deployment(name = "deploymentItemPartitionRuntime")
    public static WebArchive createDeploymentIParttionStatic() {
        WebArchive jar = ShrinkWrap.create(WebArchive.class)
                .addClass(BatchTestHelper.class)
                .addPackage(MyItemReader.class.getPackage())
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/beans.xml"))
                .addAsResource("META-INF/batch-jobs/partitionChunkRuntime.xml");
        LOG.info("{}", jar.toString(true));
        return jar;
    }

   
    
 
    
    @Test
    @OperateOnDeployment(value = "deploymentItemPartitionRuntime")
    public void testSimpleChunkCompletedPartitionStatic() throws Exception {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        long executionId = jobOperator.start("partitionChunkRuntime", new Properties());
        JobExecution jobExecution = jobOperator.getJobExecution(executionId);
        BatchTestHelper.keepTestAlive(jobExecution, 100, 1000);
        Assert.assertEquals(BatchStatus.COMPLETED, jobExecution.getBatchStatus());
//        List<StepExecution> stepExecutions = jobOperator.getStepExecutions(executionId);
//        Assert.assertTrue(!stepExecutions.isEmpty());
//        for (StepExecution stepExecution : stepExecutions) {
//            Map<Metric.MetricType, Long> metricsMap = BatchTestHelper.getMetricsMap(stepExecution.getMetrics());
//            Assert.assertEquals(Long.valueOf(5l), metricsMap.get(Metric.MetricType.READ_COUNT));
//            Assert.assertEquals(Long.valueOf(5l), metricsMap.get(Metric.MetricType.WRITE_COUNT));
//            Assert.assertEquals(Long.valueOf(3l), metricsMap.get(Metric.MetricType.COMMIT_COUNT));
//        }
    }
}
