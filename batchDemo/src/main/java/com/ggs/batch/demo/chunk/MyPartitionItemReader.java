/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemReader;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohsen
 */
@Named
public class MyPartitionItemReader implements ItemReader {
    
    private static final Logger LOG = LoggerFactory.getLogger(MyPartitionItemReader.class);
    
    @Inject
    @BatchProperty(name = "matching")
    private String matching;
    
    private Integer checkpoint;
    
    private final static List<String> ITEMS = Arrays.asList(
            "campaign1", 
            "campaign2", 
            "campaign3",
            "adgroup1",
            "adgroup2",
            "adgroup3",
            "campaign4", 
            "campaign5");
    
    public MyPartitionItemReader(){
    }

    @Override
    public void open(Serializable srlzbl) throws Exception {
        checkpoint = srlzbl == null ? 1 : (Integer) srlzbl;
        LOG.info("open-checkpoint: {}", checkpoint);
    }

    @Override
    public void close() throws Exception {
        LOG.info("close-checkpoint: {}", checkpoint);
    }

    @Override
    public Object readItem() throws Exception {
        try {
            String nextItem = matching + checkpoint;
            if (ITEMS.contains(nextItem)){
                LOG.info("readItem-checkpoint: {}, matching: {}", checkpoint, matching);
                return nextItem;
            }
            return null;
        } finally {
            checkpoint++;
        }
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        LOG.info("checkpointInfo-checkpoint: {}", checkpoint);
        return checkpoint;
    }
    
}
