/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.util.List;
import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.listener.ItemWriteListener;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohsen
 */
@Named
public class MyItemWriterListener implements ItemWriteListener {

    private static final Logger LOG = LoggerFactory.getLogger(MyItemWriterListener.class);
    
    @Inject
    @BatchProperty(name = "matching")
    private String matching;
    
    @Inject
    private StepContext stepContext;
    
    public MyItemWriterListener(){
        
    }
    
    @Override
    public void beforeWrite(List<Object> list) throws Exception {
        LOG.info("beforeWrite. Number of items: {}",list.size());
    }

    @Override
    public void afterWrite(List<Object> list) throws Exception {
        LOG.info("afterWrite. Number of items: {}, matching: {}, StepContext: {}", list.size(), matching, stepContext.hashCode());
        stepContext.setTransientUserData(new CommitSummary(matching, list));
    }

    @Override
    public void onWriteError(List<Object> list, Exception excptn) throws Exception {
        LOG.info("onWriteError. Number of items: {}", list.size());
    }
    
}
