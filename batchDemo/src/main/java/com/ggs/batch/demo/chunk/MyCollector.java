/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.batch.api.partition.PartitionCollector;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohsen
 */
@Named
public class MyCollector implements PartitionCollector{
    
    private static final Logger LOG = LoggerFactory.getLogger(MyCollector.class);
    
    @Inject
    private StepContext stepContext;
    
    @Override
    public Serializable collectPartitionData() throws Exception {
        LOG.info("collectPartitionData");
        CommitSummary transientUserData = (CommitSummary) stepContext.getTransientUserData();
        String threadName = Thread.currentThread().getName();
        List<Object> items = transientUserData.getItems();
        Map<String, String> iemToThreadNames = new HashMap<>();
        for (Object item : items) {
            iemToThreadNames.put((String) item, threadName);
        }
        return new CollectorData(iemToThreadNames);
    }
    
}
