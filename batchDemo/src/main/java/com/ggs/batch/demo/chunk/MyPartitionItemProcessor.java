/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import javax.batch.api.chunk.ItemProcessor;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohsen
 */
@Named
public class MyPartitionItemProcessor implements ItemProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(MyPartitionItemProcessor.class);

    @Override
    public Object processItem(Object object) throws Exception {
        LOG.info("processItem: {}", object);
        return (String) object + "processed";
    }

}
