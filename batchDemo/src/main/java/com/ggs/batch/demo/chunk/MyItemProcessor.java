/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.util.Properties;
import java.util.logging.Level;
import javax.batch.api.BatchProperty;
import javax.batch.api.chunk.ItemProcessor;
import javax.batch.operations.JobOperator;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.context.JobContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohsen
 */
@Named
public class MyItemProcessor implements ItemProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(MyItemProcessor.class);
    
    @Inject
    private JobContext jobContext;

    public MyItemProcessor() {
    }

    @Override
    public Object processItem(Object object) {
        JobOperator jobOperator = BatchRuntime.getJobOperator();
        long executionId = jobContext.getExecutionId();
        Properties parameters = jobOperator.getParameters(executionId);
        Integer sleepTimeFromJobContext = (Integer) parameters.get("processingSleepTime");
        LOG.info("processItem: {}, sleepTimeFromJobContext: {}", object,sleepTimeFromJobContext);
        if (sleepTimeFromJobContext!=null){
            try {
                Thread.sleep(sleepTimeFromJobContext);
            } catch (InterruptedException ex) {
                throw new RuntimeException("Current thread failed to sleep", ex);
            }
        }
        return (String) object + "processed";
    }
}
