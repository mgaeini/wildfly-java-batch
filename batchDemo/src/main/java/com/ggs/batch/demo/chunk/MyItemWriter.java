/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.io.Serializable;
import java.util.List;
import javax.batch.api.chunk.ItemWriter;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author mohsen
 */
@Named
public class MyItemWriter implements ItemWriter {

    private static final Logger LOG = LoggerFactory.getLogger(MyItemWriter.class);

    private Integer checkpoint;

    public MyItemWriter() {
    }

    @Override
    public void open(Serializable srlzbl) throws Exception {
        checkpoint = srlzbl == null ? 0 : (Integer) srlzbl;
        LOG.info("open-checkpoint: {}", checkpoint);
    }

    @Override
    public void close() throws Exception {
        LOG.info("checkpoint: {}", checkpoint);
    }

    @Override
    public void writeItems(List<Object> list) throws Exception {
        checkpoint++;
        LOG.info("writeItems-checkpoint: {}", checkpoint);
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        LOG.info("checkpointInfo-checkpoint: {}", checkpoint);
        return checkpoint;
    }

}
