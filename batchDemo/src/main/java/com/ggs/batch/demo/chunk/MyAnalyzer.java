/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.batch.api.partition.PartitionAnalyzer;
import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.context.StepContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mohsen
 */
@Named
public class MyAnalyzer implements PartitionAnalyzer {

    private static final Logger LOG = LoggerFactory.getLogger(MyAnalyzer.class);

    private Map<String, String> analyzerData = new ConcurrentHashMap<>();
    
    @Inject
    private StepContext stepContext;

    @Override
    public void analyzeCollectorData(Serializable srlzbl) throws Exception {
        LOG.info("analyzeCollectorData. StepContext: {}", stepContext.hashCode());
        analyzerData.putAll(((CollectorData) srlzbl).getItemToThreadName());
        stepContext.setTransientUserData(analyzerData);
    }

    @Override
    public void analyzeStatus(BatchStatus bs, String b) throws Exception {
        LOG.info("analyzeStatus. BatchStatus: {}, String: {}", bs, b);
    }

}
