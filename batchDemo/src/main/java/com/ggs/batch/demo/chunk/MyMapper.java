/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.util.Properties;
import javax.batch.api.partition.PartitionMapper;
import javax.batch.api.partition.PartitionPlan;
import javax.batch.api.partition.PartitionPlanImpl;
import javax.inject.Named;

/**
 *
 * @author mohsen
 */
@Named
public class MyMapper implements PartitionMapper {

    @Override
    public PartitionPlan mapPartitions() throws Exception {
        PartitionPlanImpl plan = new PartitionPlanImpl();
        plan.setPartitions(2);
        plan.setThreads(2);
        Properties properties1 = new Properties();
        properties1.put("matching", "campaign");
        Properties properties2 = new Properties();
        properties2.put("matching", "adgroup");
        plan.setPartitionProperties(new Properties[]{properties1, properties2});
        return plan;
    }

}
