/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

/**
 *
 * @author mohsen
 */
public final class CollectorData implements Serializable {

    private final Map<String, String> itemToThreadName;

    public CollectorData(Map<String, String> itemToThreadName) {
        this.itemToThreadName = Collections.unmodifiableMap(itemToThreadName);
    }

    public Map<String, String> getItemToThreadName() {
        return itemToThreadName;
    }

}
