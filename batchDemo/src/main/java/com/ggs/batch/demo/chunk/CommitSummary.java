/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mohsen
 */
public final class CommitSummary {

    private final List<Object> items;
    private final String matching;

    public CommitSummary(String matching, List<Object> items) {
        this.items = Collections.unmodifiableList(items);
        this.matching = matching;
    }

    public List<Object> getItems() {
        return items;
    }

    public String getMatching() {
        return matching;
    }

}
