/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batchapp.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.batch.runtime.BatchRuntime;
import javax.batch.runtime.BatchStatus;
import javax.batch.runtime.JobExecution;
import javax.batch.runtime.Metric;

/**
 *
 * @author mohsen
 */
public final class BatchTestHelper {

    private static final Set<BatchStatus> EXIST_STATUS = new HashSet<>(
            Arrays.asList(BatchStatus.COMPLETED, BatchStatus.FAILED, BatchStatus.STOPPED));

    private BatchTestHelper() {
        throw new UnsupportedOperationException();
    }

    public static JobExecution keepTestAlive(JobExecution jobExecution, int maxTries, long threadSleep) throws InterruptedException {
        int numTries = 0;
        while (!EXIST_STATUS.contains(jobExecution.getBatchStatus())) {
            if (numTries < maxTries) {
                numTries++;
                Thread.sleep(threadSleep);
                jobExecution = BatchRuntime.getJobOperator().getJobExecution(jobExecution.getExecutionId());
            } else {
                break;
            }
        }
        return jobExecution;
    }

    public static Map<Metric.MetricType, Long> getMetricsMap(Metric[] metrics) {
        Map<Metric.MetricType, Long> metricsMap = new HashMap<>();
        for (Metric metric : metrics) {
            metricsMap.put(metric.getType(), metric.getValue());
        }
        return metricsMap;
    }
}
