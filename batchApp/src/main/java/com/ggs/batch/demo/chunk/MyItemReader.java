/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ggs.batch.demo.chunk;

import javax.inject.Named;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.batch.api.chunk.ItemReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author mohsen
 */
@Named
public class MyItemReader implements ItemReader {

    private static final Logger LOG = LoggerFactory.getLogger(MyItemReader.class);

    private Integer checkpoint;

    private final static List<String> IDS = Arrays.asList("Item1", "Item2", "Item3", "Item4", "Item5");

    public MyItemReader() {

    }

    @Override
    public Object readItem() throws Exception {
        LOG.info("readItem-checkpoint: {}", checkpoint);
        return checkpoint != IDS.size() ? IDS.get(checkpoint) : null;
    }

    @Override
    public void open(Serializable srlzbl) throws Exception {
        checkpoint = srlzbl == null ? 0 : (Integer) srlzbl;
        LOG.info("open-checkpoint: {}", checkpoint);
    }

    @Override
    public void close() throws Exception {
        LOG.info("close-checkpoint: {}", checkpoint);
    }

    @Override
    public Serializable checkpointInfo() throws Exception {
        checkpoint++;
        LOG.info("checkpointInfo-checkpoint: {}", checkpoint);
        return checkpoint;
    }

}
